<?php
namespace PostalCodes\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PostalCodes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $States
 *
 * @method \App\Model\Entity\PostalCode get($primaryKey, $options = [])
 * @method \App\Model\Entity\PostalCode newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PostalCode[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PostalCode|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PostalCode patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PostalCode[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PostalCode findOrCreate($search, callable $callback = null, $options = [])
 */
class PostalCodesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->belongsTo('States', [
            'foreignKey' => 'state_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->requirePresence('cp', 'create')
            ->notEmptyString('cp');

        $validator
            ->requirePresence('colonia', 'create')
            ->notEmptyString('colonia');

        $validator
            ->requirePresence('municipio', 'create')
            ->notEmptyString('municipio');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['state_id'], 'States'));

        return $rules;
    }
}
