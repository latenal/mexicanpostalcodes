<?php

namespace PostalCodes\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

class PostalCodesComponent extends Component
{
    protected $_config;


    /**
     * Returns a query with all states
     *
     * @return \Cake\ORM\Query
     */
    public function states(): Query
    {
        $statesTable = TableRegistry::getTableLocator()->get('PostalCodes.States');
        $states = $statesTable->find();

        return $states;
    }


    /**
     * Returns a query with a list of municipalities by state ID
     *
     * @param null $state_id
     * @return \Cake\ORM\Query
     */
    public function municipalities($state_id = null): Query
    {
        $postalCodesTable = TableRegistry::getTableLocator()->get('PostalCodes.PostalCodes');
        $mpos = $postalCodesTable->find()
            ->select(['municipio'])
            ->distinct(['municipio'])
            ->enableHydration(false)
            ->where(['state_id' => $state_id])
            ->orderAsc('municipio')
            ->formatResults(function ($results) {
                return $results->extract('municipio');
            });

        return $mpos;
    }


    /**
     * Returns a query with a list of colonies by state ID and municipality name
     *
     * @param int $state_id an ID of a state
     * @param string $municipality a municipality name
     * @return \Cake\ORM\Query
     */
    public function colonies($state_id, $municipality): Query
    {
        $postalCodesTable = TableRegistry::getTableLocator()->get('PostalCodes.PostalCodes');
        $colonies = $postalCodesTable->find()
            ->select(['colonia'])
            ->distinct(['colonia'])
            ->enableHydration(false)
            ->where([
                'state_id' => $state_id,
                'municipio' => $municipality
            ])
            ->orderAsc('colonia')
            ->formatResults(function ($results) {
                return $results->extract('colonia');
            });

        return $colonies;
    }


    /**
     * Returns a query with a state, state id and muncipality name for a specified postal code
     *
     * @param string $postalCode a postal code (5 digits)
     * @return \Cake\ORM\Query
     */
    public function getStateAndMuncipalityByPostalCode($postalCode): Query
    {
        $postalCodesTable = TableRegistry::getTableLocator()->get('PostalCodes.PostalCodes');
        $q = $postalCodesTable->find()
            ->select(['municipio', 'state_id'])
            ->contain(['States' => function ($q) {
                return $q->select('name');
            }])
            ->where([
                'cp LIKE' => $postalCode
            ])
            ->limit(1);

        return $q;
    }


    /**
     * Returns a list of colonies for specified postal codes
     * @param string $postalCode a postal code (5 digits)
     * @return \Cake\ORM\Query
     */
    public function getColoniesByPostalCode($postalCode): Query
    {
        $postalCodesTable = TableRegistry::getTableLocator()->get('PostalCodes.PostalCodes');
        $colonias = $postalCodesTable->find()
            ->select('colonia')
            ->distinct()
            ->where([
                'cp LIKE' => $postalCode
            ]);

        return $colonias;
    }
}
