# AlusCatalogue plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](http://getcomposer.org).

### 1. Install the plugin via composer.

Add to `composer.json`:
```
composer require latenal/mexican-postal-codes
```
After that, call: 
```
composer install
```

### 2. Load plugin in cake:
```
bin/cake plugin load PostalCodes
```

### 3. Then, apply migrations and seeds:
```
bin/cake migrations migrate --plugin PostalCodes
bin/cake migrations seed --plugin PostalCodes
```



## Usage
In your controller add:
```
$this->loadComponent('PostalCodes.PostalCode', []);
```


#### Get a list of states
```
$states = $this->PostalCode->states()->toArray();
```


#### Get a list of municipalities by state id
```
$state_id = 1;
$municipalities = $this->PostalCode->municipalities($state_id)->toArray();
```


#### Get a list of colonies by state id and municipality name
```
$state_id = 1;
$municipality = 'Álvaro Obregón';
$colonies = $this->PostalCode->colonies($state_id, $municipality)->toArray();
```


#### Get a state and municipality by postal code
```
$postal = '06760';
$addr = $this->PostalCode->getStateAndMuncipalityByPostalCode($postal)->first();
```


#### Get a list of colonies by postal code
```
$postal = '06760';
$colonies = $this->PostalCode->getColoniesByPostalCode($postal)->toArray();
```
