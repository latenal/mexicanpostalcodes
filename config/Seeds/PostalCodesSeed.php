<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * CitiesSeed seed.
 */
class PostalCodesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $this->execute(file_get_contents(realpath(dirname(__FILE__))  . '/postal_codes.sql'));
    }
}
