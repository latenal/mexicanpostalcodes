<?php

use Migrations\AbstractMigration;

class UpdatePostalCodes extends AbstractMigration
{
    public function up()
    {
        $this->execute(file_get_contents(realpath(dirname(__FILE__)) . '/../Seeds/20201004005451_postal_codes.sql'));
    }
}
