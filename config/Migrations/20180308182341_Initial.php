<?php

use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('states');
        $table
            ->addColumn('name', 'string', [
                'default' => '',
                'limit' => 150,
                'null' => false,
            ])
            ->create();


        $table = $this->table('postal_codes');
        $table
            ->addColumn('cp', 'string', [
                'default' => '',
                'limit' => 5,
                'null' => false,
            ])
            ->addColumn('colonia', 'string', [
                'default' => '',
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('municipio', 'string', [
                'default' => '',
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('state_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->create();
    }

    public function down()
    {
        $this->dropTable('states');
        $this->dropTable('postal_codes');
    }
}
